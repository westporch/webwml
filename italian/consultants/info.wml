#use wml::debian::template title="Informazioni per consulenti Debian"
#use wml::debian::translation-check translation="b27d50a764c00727d11d19b785f566eff3b2fa87" maintainer="Giuseppe Sacco"

<h2>Regolamento per la pagina dei consulenti Debian</h2>

<p>Per essere aggiunti all'elenco dei consulenti del sito web Debian devono
essere seguite le seguenti regole:</p>

<ul>
  <li>
	Informazioni obbligatorie<br />
	Si deve fornire un indirizzo email valido e rispondere ai messaggi di posta
	elettronica entro quattro settimane. Al fine di evitare abusi le
richieste (integrazioni, rimozioni o modifiche) devono essere inviate
dallo stesso indirizzo. Per evitare lo spam, è possibile chiedere che
l'indirizzo email sia camuffato (per esempio <q>debian -dot- consulting -at-
example -dot- com</q>); per avere l'indirizzo email camuffato farne
esplicitamente richiesta nella candidatura. In alternativa è possibile
richiedere che l'indirizzo email non sia visibile nella pagina web
(in questo caso è necessario un indirizzo email funzionante per la
manutenzione dell'elenco). Per tenere nascosto il proprio indirizzo è
possibile anche fornire l'URL della pagina dei contatti sul proprio sito
web, utilizzando <q>Contact</q>.
  </li>
  <li>
	Se si fornisce un collegamento ad un sito web, quel sito deve menzionare
	l'attività di consulenza su Debian. È molto apprezzato, anche
	se non obbligatorio, fornire il collegamento diretto al posto di quello alla
	pagina principale del sito. Ma solo se l'informazione è comunque
	ragionevolmente accessibile.
  </li>
  <li>
	Più città/regioni/nazioni<br />
	Ciascun consulente ha la possibilità di scegliere in quale nazione (una
	soltanto) voglia essere elencato. Si può inserire un solo indirizzo
	da pubblicare ma ovviamente si possono inserire altre città o
	nazioni nelle informazioni addizionali o nel proprio sito web.
  </li>
  <li>
        Regole per sviluppatori Debian<br />
        Non è permesso utilizzare l'indirizzo ufficiale @debian.org
	per l'elenco dei consulenti.<br />
	Se viene fornito un URL, non può essere nel dominio
	ufficiale *.debian.org.<br />
	L'obbligo deriva dalla <a href="$(HOME)/devel/dmup">DMUP</a>.
</ul>

<p>Se le regole sopra elencate non sono soddisfatte, il consulente
riceverà un email di notifica riguardo la rimozione dall'elenco a meno
che non torni a soddisfare i criteri entro un periodo di quattro settimane.</p>

<p>Le informazioni possono essere parzialmente (o totalmente) rimosse se non
soddisfano più il regolamento, o comunque a discrezione dei gestori
dell'elenco.</p>

<h2>Aggiunte, modifiche e cancellazione dei consulenti</h2>

<p>Se si vuole che la propria società sia inserita in queste pagine, si
deve mandare una email all'indirizzo <a
href="mailto:consultants@debian.org">consultants@debian.org</a>, in inglese,
con le informazioni tra quelle sotto specificate che volete che siano
mostrate (l'indirizzo email è obbligatorio, tutto il resto è facoltativo e
a vostra discrezione):</p>

<ul>
  <li>Country for which you want to be listed (Stato per il quale si vuole
     essere inseriti)</li>
  <li>Name (Nome)</li>
  <li>Company (Società)</li>
  <li>Address (Indirizzo)</li>
  <li>Phone (Telefono)</li>
  <li>Fax</li>
  <li>Contact (Contatto)</li>
  <li>E-mail</li>
  <li>URL</li>
  <li>Rates (Tariffe)</li>
  <li>Additional information, if any (Altre eventuali informazioni)</li>
</ul>

<p>Una richiesta di aggiornamento deve essere inviata a 
<a href="mailto:consultants@debian.org">consultants@debian.org</a>,
preferibilmente dallo stesso indirizzo email pubblicato nella pagina dei
consulenti (<a href="https://www.debian.org/consultants/">\
https://www.debian.org/consultants/</a>).</p>

<p>Tutte le informazioni fornite verranno mostrare nell'elenco dei
consulenti. In particolare l'indirizzo email verrà mostrato
e sarà disponibile ad ogni utente. Se si vuole utilizzare un
indirizzo email offuscato (scrambled), lo si deve esplicitare nella
richiesta di ammissione.</p>

<p>I consulenti possono comunicare con altri consulenti Debian consultants
con la <a href="https://lists.debian.org/debian-consultants/">lista di
messagfi debian-consultants</a>.</p>
