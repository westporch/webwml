#use wml::debian::template title="Siti mirror di Debian in tutto il mondo" BARETITLE=true
#use wml::debian::translation-check translation="54e7b1c853358a6c386f31e4ac3a5d90a6f3890f" maintainer="skizzhg"

<p>Debian è distribuita (<em>mediante mirror</em>) su centinaia di
server su internet. L'utilizzo di un server vicino dovrebbe aumentare
la velocità di download, riducendo anche il carico sui nostri server
centrali e su internet in generale.</p>

<p class="centerblock">
Esistono mirror in molti paesi, per alcuni di questi è stato aggiunto
l'alias <code>ftp.&lt;paese&gt;.debian.org</code>. A questi alias
puntano a dei mirror che si sincronizzano regolarmente e velocemente
e dispongono di tutte le architetture. L'archivio Debian è sempre
disponibile via <code>HTTP</code> e nella posizione <code>/debian</code>.
</p>

<p class="centerblock">
Altri <strong>siti mirror</strong> possono avere delle restrizioni su
cosa ospitano (a causa di limiti di spazio). Solo perché un mirror non
è <code>ftp.&lt;paese&gt;.debian.org</code> non significa necessariamente
che sia più lento o meno aggiornato di
<code>ftp.&lt;paese&gt;.debian.org</code>. Infatti un mirror che dispone
della propria architettura, che è più vicino all'utente e, quindi, più
veloce è preferibile rispetto agli altri mirror più lontani.
</p>

<p>Utilizzare il sito a voi più vicino per ottenere download più veloci
indipendentemente che abbia o non abbia l'alias per paese.
Per determinare il sito con meno latenza si può utilizzare il programma
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a>; per determinare il sito con il maggior rendimento
si usi un programma come 
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> o
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a>.
Si noti che la vicinanza geografica è spesso il fattore più importante
per determinare quale macchina offrirà il servizio migliore.</p>

<p>Se il sistema viaggia molto, potrebbe essere opportuno utilizzare
un <q>mirror</q> servito tramite una <abbr
title="Content Delivery Network">CDN</abbr> globale. A tale scopo
il progetto Debian gestisce <code>deb.debian.org</code> e quindi è
possibile usarlo nel proprio apt sources.list; consultare il <a
href="http://deb.debian.org/">sito web del servizio per i dettagli</a>.
</p>

<p>La copia autorevole del seguente elenco si trova alla pagina:
<url "https://www.debian.org/mirror/list">.
Qualsiasi altro dettaglio sui mirror Debian si trova alla pagina:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Alias dei mirror Debian per paese</h2>

<table border="0" class="center">
<tr>
  <th>Paese</th>
  <th>Sito</th>
  <th>Architetture</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Elenco dei mirror dell'archivio Debian</h2>

<table border="0" class="center">
<tr>
  <th>Nome dell'host</th>
  <th>HTTP</th>
  <th>Architetture</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
