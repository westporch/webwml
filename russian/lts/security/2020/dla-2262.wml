#use wml::debian::translation-check translation="d865f76ebc86ab1cdd68218b3fbc1c9291f641fd" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности LTS</define-tag>
<define-tag moreinfo>

<p>В qemu, быстром эмуляторе процессора, было исправлено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1983">CVE-2020-1983</a>

    <p>slirp: исправление использования указателей после освобождения памяти в ip_reass().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13361">CVE-2020-13361</a>

    <p>es1370_transfer_audio в hw/audio/es1370.c
    позволяет пользователям гостевой системы обращаться за пределы выделенного буфера
    памяти в ходе выполнения операции es1370_write().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13362">CVE-2020-13362</a>

    <p>megasas_lookup_frame в hw/scsi/megasas.c может выполнять чтение
     за пределами выделенного буфера памяти из-за специально сформированного поля
     reply_queue_head, полученного от пользователя гостевой системы.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13765">CVE-2020-13765</a>

    <p>hw/core/loader: исправление аварийной остановки в rom_copy().</p></li>

</ul>

<p>В Debian 8 <q>Jessie</q> эти проблемы были исправлены в версии
1:2.1+dfsg-12+deb8u15.</p>

<p>Рекомендуется обновить пакеты qemu.</p>

<p>Дополнительную информацию о рекомендациях по безопасности Debian LTS,
о том, как применять эти обновления к вашей системе, а также ответы на часто
задаваемые вопросы можно найти по адресу: <a href="https://wiki.debian.org/LTS">\
https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2262.data"
