#use wml::debian::translation-check translation="8893b6716ac8e06f1696d121568a974bf244f9c3" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В OpenSSL, наборе инструментов протокола защиты информации, были
обнаружены многочисленные уязвимости.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3711">CVE-2021-3711</a>

    <p>Джон Оуян сообщил о переполнении буфера в расшифровке SM2.
    Злоумышленник, способный представить SM2-содержимое для расшифровки
    приложению, может использовать эту уязвимость для изменения
    поведения приложения или вызова его аварийной остановки
    (отказ в обслуживании).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3712">CVE-2021-3712</a>

    <p>Инго Шварце сообщил о переполнении буфера при обработке строк ASN.1
    в функции X509_aux_print(), которое может приводить к отказу
    в обслуживании.</p>

<p>Дополнительные подробности можно найти в рекомендации основной ветки разработки:
<a href="https://www.openssl.org/news/secadv/20210824.txt">\
https://www.openssl.org/news/secadv/20210824.txt</a></p></li>

</ul>

<p>В предыдущем стабильном выпуске (buster) эти проблемы были исправлены
в версии 1.1.1d-0+deb10u7.</p>

<p>В стабильном выпуске (bullseye) эти проблемы были исправлены в
версии 1.1.1k-1+deb11u1.</p>

<p>Рекомендуется обновить пакеты openssl.</p>

<p>С подробным статусом поддержки безопасности openssl можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4963.data"
