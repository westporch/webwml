# translation of security.ru.po to Russian
# Yuri Kozlov <yuray@komyakino.ru>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml security\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2018-11-04 21:40+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "Безопасность Debian"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "Рекомендации Debian по безопасности"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "Вопрос"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr ""
"<a href=\\\"undated/\\\">Не датированные</a> рекомендации по безопасности, "
"оставленные для потомков"

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "В каталоге Mitre CVE"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "База данных Securityfocus Bugtraq"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "Рекомендации CERT"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "Информация об уязвимостях US-CERT"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "Исходный код:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "Независимые от архитектуры компоненты:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"Контрольные суммы MD5 этих файлов доступны в <a href=\"<get-var url />"
"\">исходном сообщении</a>."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"Контрольные суммы MD5 этих файлов доступны в <a href=\"<get-var url />"
"\">пересмотренном сообщении</a>."

#: ../../english/template/debian/security.wml:44
msgid "Debian Security Advisory"
msgstr "Рекомендация Debian по безопасности"

#: ../../english/template/debian/security.wml:49
msgid "Date Reported"
msgstr "Дата сообщения"

#: ../../english/template/debian/security.wml:52
msgid "Affected Packages"
msgstr "Затронутые пакеты"

#: ../../english/template/debian/security.wml:74
msgid "Vulnerable"
msgstr "Уязвим"

#: ../../english/template/debian/security.wml:77
msgid "Security database references"
msgstr "Ссылки на базы данных по безопасности"

#: ../../english/template/debian/security.wml:80
msgid "More information"
msgstr "Более подробная информация"

#: ../../english/template/debian/security.wml:86
msgid "Fixed in"
msgstr "Исправлено в"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "Идентификатор BugTraq"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "Ошибка"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "В системе отслеживания ошибок Debian:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr "В базе данных Bugtraq (на SecurityFocus):"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "В каталоге Mitre CVE:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "База данных CERT по уязвимостям, предложениям и инцидентам:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr ""
"На данный момент ссылки на внешние базы данных по безопасности отсутствуют."
