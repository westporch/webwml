<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was discovered in the Dovecot email server.
When reading FTS headers from the Dovecot index, the input buffer
size is not bounds-checked. An attacker with the ability to modify
dovecot indexes, can take advantage of this flaw for privilege
escalation or the execution of arbitrary code with the permissions of
the dovecot user. Only installations using the FTS plugins are affected.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.2.13-12~deb8u6.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1736.data"
# $Id: $
