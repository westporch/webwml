<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap-based buffer overflow was discovered in NTFS-3G, a read-write
NTFS driver for FUSE. A local user can take advantage of this flaw for
local root privilege escalation.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2014.2.15AR.2-1+deb8u4.</p>

<p>We recommend that you upgrade your ntfs-3g packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1724.data"
# $Id: $
