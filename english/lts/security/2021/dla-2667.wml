<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in djvulibre, a library and
set of tools to handle documents in the DjVu format. An attacker could
crash document viewers and possibly execute arbitrary code through
crafted DjVu files.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.5.27.1-7+deb9u1.</p>

<p>We recommend that you upgrade your djvulibre packages.</p>

<p>For the detailed security status of djvulibre please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/djvulibre">https://security-tracker.debian.org/tracker/djvulibre</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2667.data"
# $Id: $
