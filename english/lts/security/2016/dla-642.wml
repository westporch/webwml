<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Active Record in Ruby on Rails 4.1.x before 4.1.14.1, 4.2.x before
4.2.5.1, and 5.x before 5.0.0.beta1.1 supports the use of instance-level
writers for class accessors, which allows remote attackers to bypass
intended validation steps via crafted parameters.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.6-5+deb7u3.</p>

<p>We recommend that you upgrade your ruby-activerecord-3.2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-642.data"
# $Id: $
