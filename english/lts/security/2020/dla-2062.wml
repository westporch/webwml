<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that sa-exim, the SpamAssassin filter for Exim, allows
attackers to execute arbitrary code if users are allowed to run custom
rules. A similar issue was fixed in spamassassin, <a href="https://security-tracker.debian.org/tracker/CVE-2018-11805">CVE-2018-11805</a>, which
caused a functional regression in sa-exim. This update restores the
compatibility between spamassassin and sa-exim. The security
implications of sa-exim's greylisting function are also documented in
/usr/share/doc/sa-exim/README.greylisting.gz.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.2.1-14+deb8u1.</p>

<p>We recommend that you upgrade your sa-exim packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2062.data"
# $Id: $
