#use wml::debian::translation-check translation="399626927b999b3938582bfb0243645dfed48f14" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Flera sårbarheter har upptäckts i cURL, ett URL-överföringsbibliotek.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16890">CVE-2018-16890</a>

	<p>Wenxiang Qian från Tencent Blade Team upptäckte att funktionen
	som hanterar inkommande NTLM typ-2-meddelanden inte validerar inkommande
	data korrekt och är sårbar för ett heltalsspill, som kunde leda till
	en läsning utanför bufferten.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3822">CVE-2019-3822</a>

	<p>Wenxiang Qian från Tencent Blade Team upptäckte att funktionen
	som skapar en utgående NTLM typ-2-rubrik är sårbar för ett
	heltalsspill, vilket kunde leda till en skrivning utanför gränserna.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3823">CVE-2019-3823</a>

	<p>Brian Carpenter från Geeknik Labs upptäckte att koden som hanterar
	end-of-response för SMTP är sårbar för en läsning av heap utanför
	gränserna.</p></li>

</ul>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 7.52.1-5+deb9u9.</p>

<p>Vi rekommenderar att ni uppgraderar era curl-paket.</p>

<p>För detaljerad säkerhetsstatus om curl vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4386.data"
