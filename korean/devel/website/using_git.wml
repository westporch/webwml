#use wml::debian::template title="git 사용하여 웹사이트 소스 코드 다루기"
#use wml::debian::translation-check translation="5ec84105c40bf62785fc1159fcf33ed470a2fbc7" maintainer="Sebul"

<h2>소개</h2>

<p>git은 <a href="https://en.wikipedia.org/wiki/Version_control">version
control system</a>이며,
같은 재료에서 여러 사람이 동시에 일하는 것을 다루는 것을 돕는 프로그램입니다.
모든 사용자는 주 저장소의 로컬 사본을 만들 수 있습니다.
로컬 사본은 같은 컴퓨터, 또는 세계에 있을 수 있습니다.
사용자는 로컬 사본을 원하는 대로 바꾸고, 바뀐 재료가 준비되면, 변경을 커밋하고 
그것을 주 저장소에 다시 push 합니다.</p>

<p>git은 원격 저장소에 같은 브랜치에 있는 여러분의 로컬 사본보다 새로운 커밋(변경)이 있으면 파일을 직접 push 못 하게 합니다.
이 경우 충돌이 일어나는 경우, 먼저 여러분의 로컬 사본을 가져와서 업데이트 하고 여러분의 새 변경을 최근 커밋의 꼭대기에서 <code>rebase</code> 하세요.
</p>

<h3><a name="write-access">git 저장소 쓰기 접근</a></h3>
<p>
전체 데비안 웹사이트 소스 코드는 git에서 관리합니다.
<url https://salsa.debian.org/webmaster-team/webwml/>에 있습니다.
기본적으로, 게스트는 소스 코드 저장소로 커밋을 push 할 수 없습니다.
저장소에 대한 쓰기 접근 권한을 얻으려면 일종의 권한이 필요합니다.
</p>

<h4><a name="write-access-unlimited">무제한 쓰기 접근</a></h4>
<p>저장소에 대한 무제한 쓰기 접근(예: 사용자가 자주 참여하게 될 경우)이 필요하면, 
데비안 Salsa 플랫폼에 로그인 후 <url https://salsa.debian.org/webmaster-team/webwml/> 웹 인터페이스를 통해 쓰기 접근을 요청하는 것을 고려하세요.
</p>

<p>데비안 웹사이트 개발에 처음이고 이전 경험이 없으면, 무제한 쓰기 접근을 요청하기 전에 
자기 소개서와 함께 <a href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>에 메일을 보내세요.
여러분이 소개할 때, 어떤 언어 또는 웹사이트의 어느 부분을 사용할 계획이며, 누가 여러분을 보증할 것인지와 같은 유용한 정보를 제공하세요.
</p>

<h4><a name="write-access-via-merge-request">Merge Request 통해 저장소에 쓰기</a></h4>
<p>
저장소에 대한 무제한 쓰기 접근을 바라지 않거나 접근할 수 없는 경우, 
언제든지 Merge Request 하고 다른 개발자가 작업을 검토하고 여러분의 작업을 수락하도록 할 수 있습니다.
Salsa GitLab 플랫폼에서 제공하는 표준 절차를 사용하여 웹 인터페이스를 통해 Merge Request 하세요.
</p>

<p>
Merge Request를 모든 웹사이트 개발자가 모니터하지 않으므로 시간 안에 시간 내에 처리되지 않을 수 있습니다.
여러분의 기여가 적용될지 확실하지 않으면 <a href="https://lists.debian.org/debian-www/">debian-www</a> 메일링리스트에 메일을 보내고 검토를 요청하세요.
</p>

<h2><a name="work-on-repository">저장소에서 작업하기</a></h2>

<h3><a name="clone-local-repo-copy">저장소의 로컬 사본 복제</a></h3>

<p>우선, 저장소에서 작업하려면 git을 설치해야 합니다.(일반적인 git 문서 참조)
다음, 둘 중 한 방법으로 저장소를 복제(다른 말로, 로컬 사본 만들기)합니다.</p>

<p>webwml에서 작업하기 위한 권장 방법은 먼저 salsa.debian.org에 계정을 등록하고 
SSH 공용 키를 사용자의 Salsa 계정에 업로드하여 git SSH 접근을 활성화하는 것입니다.
더 자세한 정보는
<a
href="https://salsa.debian.org/help/ssh/README.md">salsa 도움말 페이지</a>를 보세요.
webwml 저장소를 아래 명령으로 복제할 수 있습니다:</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>salsa 계정이 없으면, 다른 방법은 HTTPS 프로토콜을 써서 저장소를 복제:</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>이러면 로컬에서 똑같은 저장소 복사본이 제공되지만, 변경 내용을 직접 이 방식으로 다시 push할 수는 없습니다.</p>

<p>전체 Webwml 저장소를 복제하려면 약 500MB의 데이터를 다운로드해야 하므로 인터넷 연결이 느리거나 불안정하면 어려울 수 있습니다.
먼저 최소 깊이의 얕은 복제를 시도하여 초기 다운로드를 줄일 수 있습니다:</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>사용 가능한(얕은) 저장소를 가져온 다음, 로컬 얕은 복사본을 심층화하고 최종적으로 전체 로컬 저장소로 변환할 수 있습니다.:</p>

<pre>
  git fetch --deepen=1000 # deepen the repo for another 1000 commits
  git fetch --unshallow   # fetch all missing commits, convert the repo to a complete one
</pre>

<h4><a name="partial-content-checkout">부분 content checkout</a></h4>

<p>페이지의 부분집합만을 위한 checkout을 만들수 있습니다:</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
   In webwml: Create the file .git/info/sparse-checkout with content like this
   (if you only want the base files, English, Catalan and Spanish translations):
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
   Then:
   $ git checkout --
</pre>

<h3><a name="submit-changes">로컬 변경 제출</a></h3>

<h4><a name="keep-local-repo-up-to-date">로컬 저장소 최신 유지</a></h4>

<p>며칠마다 (그리고 적어도 뭔가 편집 하기 전!) 아래와 같이 해서</p>

<pre>
   git pull
</pre>

<p>변경이 있는 저장소에서 파일을 찾길 바랄 겁니다.</p>

<p>
"git pull"을 수행하고 편집 작업을 수행하기 전에 로컬 git working 디렉터리를 깨끗하게 유지하는 것이 좋습니다.
현재 브랜치의 원격 저장소에 없는 커밋되지 않은 변경 사항이나 로컬 커밋이 있는 경우 "git pull"을 수행하면 자동으로 병합 커밋이 생성되거나 충돌로 인해 실패합니다. 
완료되지 않은 작업을 다른 브랜치에 보관하거나 "git stash"와 같은 명령을 사용하는 것을 고려해 주십시오.</p>

<p>주의: git은 분산(중앙 집중화되지 않은) 버전 제어 시스템입니다. 
즉, 변경을 커밋할 때 변경 내용이 로컬 저장소에만 저장됩니다.
다른 사람과 공유하려면 변경 내용을 Salsa의 중앙 저장소에 push 해야 합니다.</p>

<h4><a name="example-edit-english-file">영어 파일 편집 예</a></h4>

<p>웹사이트 소스 코드 저장소 안의 영어 파일을 편집하는 예를 여기 제공합니다.
"git clone"을 써서 저장소의 로컬 사본을 얻은 후 작업을 편집하기 전에, 아래 명령을 치세요.
</p>

<pre>
   $ git pull
</pre>

<p>파일을 편집하세요. 다 되면, 여러분의 변경 사항을 저장소에 커밋할 수 있습니다.</p>

<pre>
   $ git add path/to/file(s)
   $ git commit -m "Your commit message"
</pre>

<p>원격 webwml 저장소에 대한 무제한 쓰기 접근 권한이 있다면, 이제 변경 내용을 Salsa 저장소에 직접 push 할 수 있습니다:</p>

<pre>
   $ git push
</pre>

<p>webwml 저장소에 직접 쓰기 권한이 없다면 Salsa GitLab 플랫폼에서 제공하는 Merge Request 기능을 사용하여 변경 사항을 제출하거나 다른 개발자에게 도움을 요청하는 게 좋습니다.
</p>

<p>지금까지는 어떻게 git을 이용하여 데비안 웹 사이트의 소스
코드를 조작하는지 아주 기본적인 요약입니다.
git에 대한 자세한 내용은 git 문서를 읽어보십시오.</p>

### FIXME: Is the following still true? holgerw
### FIXME: Seems not true anymore. Needs fixup. -- byang
<h4><a name="closing-debian-bug-in-git-commits">git commit에서 데비안 버그 닫기</a></h4>

<p>
커밋 로그 항목에 <code>Closes: #</code><var>nnnnnn</var>를 포함하면 
변경을 push 하면 버그 번호 <code>#</code><var>nnnnnnn</var>가 자동으로 닫힙니다.
이것의 정확한 형식은
<a href="$(DOC)/debian-policy/ch-source.html#id24">데비안 정책</a>에서와 같습니다.
</p>

<h4><a name="links-using-http-https">HTTP/HTTPS 사용한 링크</a></h4>

<p>많은 데비안 웹 사이트가 SSL/TLS를 지원하므로, 가능하고 합리적인 곳에 HTTPS 링크를 사용하세요.
<strong>그러나</strong>,
일부 Debian/DebConf/SPI/etc 웹사이트는 HTTPS를 지원하지 않거나 SPI CA만 사용합니다.
(SPI CA는 모든 브라우저에서 신뢰하는 SSL CA가 아닙니다.)
데비안 안 쓰는 사용자에게 오류 메시지 나오지 않게 하려면, 그런 사이트를 https 써서 링크하지 마세요.
</p>

<p>git 저장소는 https를 지원하거나 Debian/DebConf/SPI 웹사이트를 위한 https 링크를 포함한 
https를 지원하지 않는 것으로 알려지거나 SPI에 의해 인증 받은 데비안 웹사이트에 대한
일반 HTTP 링크를 포함하는 커밋을 거부할 겁니다.</p>

<h3><a name="translation-work">번역 작업</a></h3>

<p>번역은 항상 해당 영어 파일에 최신 상태로 유지해야 합니다.
번역 파일의 "translation-check" 헤더는 
현재 번역이 기반한 영어 파일의 어떤 버전을 기반으로 했는지 추적하는 데 쓰입니다.
번역된 파일을 변경한다면, translation-check 헤더를 업데이트하여 
해당 변경 내용의 영어 파일 안에서 해당 변경의 git commit hash와 맞추어야 합니다.
그 hash를 찾으려면</p>

<pre>
$ git log path/to/english/file
</pre>

<p>파일의 새 번역을 한다면, <q>copypage.pl</q> 스크립트를 쓰면 여러분 언어의 템플릿이 만들어지며,
올바른 번역 헤더를 포함합니다.</p>

<h4><a name="translation-smart-change">smart_change.pl로 번역 변경</a></h4>

<p><code>smart_change.pl</code>은 원본 파일과 해당 번역을 함께 업데이트하기 쉽게 하기
위한 스크립트입니다. 두 가지 방법이 있는데, 무엇을 바꾸냐에 따라 다릅니다.</p>

<p><code>smart_change</code> 쓰려면 파일에서 수동 작업할 때 
translation-check header를 업데이트하세요:</p>

<ol>
  <li>원본 파일 변경 만들고, 커밋</li>
  <li>번역 업데이트</li>
  <li>smart_change.pl 돌리기 - 번역 파일에서 변경 확인하고 헤더를 업데이트</li>
  <li>변경 검토 (예를 들어 <q>git diff </q>)</li>
  <li>번역 변경을 커밋</li>
</ol>

<p>smart_change를 정규표현식과 함께 사용하여 여러 파일을 한번에 바꿀 때:</p>

<ol>
  <li><code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code> 돌리기</li>
  <li>변경 검토 (e.g. <code>git diff</code>)
  <li>원본 파일 커밋</li>
  <li><code>smart_change.pl origfile1 origfile2</code> 돌리기
    (i.e. 이번엔 <strong>regexp 없이</strong> );
    번역 파일 헤더만 업데이트</li>
  <li>마지막으로, 번역 변화 커밋</li>
</ol>

<p>이건 이전보다 복잡(두 번 커밋 필요)하지만,
git commit hash 방식때문에 불가피합니다.</p>

<h2><a name="notifications">알림 받기</a></h2>

<h3><a name="commit-notifications">commit 알림 받기</a></h3>

<p>Salsa에서 프로젝트의 구성을 설정했으므로, 커밋은 IRC 채널 #debian-www에 보입니다.
</p>

<p>webwml 저장소 커밋이 있을 때 이메일을 통해 알림을 받으려면,
<q>www.debian.org</q> pseudopackage에 tracker.debian.org 통해 가입하고 
거기서 <q>vcs</q> 키워드를 활성화하면(아래 단계 한 번만) 됩니다:
</p>

<ul>
  <li>웹브라우저를 열고 <url https://tracker.debian.org/pkg/www.debian.org>로 갑니다</li>
  <li><q>www.debian.org</q> pseudopackage 가입. (SSO 통해 인증하거나 이메일과 암호 등록,
다른 목적으로 이미 tracker.debian.org를 쓰지 않으면)</li>
  <li><url https://tracker.debian.org/accounts/subscriptions/>로 가서, 
  <q>키워드 바꾸기</q>,  <q>vcs</q>체크 (체크 안 되어 있으면) 그리고 저장.</li>
  <li>이제 누군가 webwml 저장소에 커밋할 때 이메일을 받을 겁니다.
우리는 다른 webmaster-team을 추가할 겁니다.</li>
</ul>

<h3><a name="merge-request-notifications">Merge Request 알림 받기</a></h3>

<p>Salsa GitLab 플랫폼의 webwml 저장소 웹 인터페이스에 제출된 새로운 Merge Request 마다
알림 이메일을 받으려면 웹 인터페이스의 webwml 저장소에 대한 알림 설정을 아래 단계에 따라 구성하세요:
</p>

<ul>
  <li>Salsa 계정에 로그인 해서 프로젝트 페이지 가기;</li>
  <li>프로젝트 홈페이지 꼭대기에서 종 아이콘 클릭;</li>
  <li>여러분이 좋아하는 알림 수준 선택.</li>
</ul>
