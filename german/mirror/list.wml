#use wml::debian::template title="Weltweite Spiegel-Sites von Debian" BARETITLE=true
#use wml::debian::translation-check translation="54e7b1c853358a6c386f31e4ac3a5d90a6f3890f"
# $Id$
# Translator: Helge Kreutzmann <debian@helgefjell.de>, 2007-11-25
# Updated: Holger Wansing <linux@wansing-online.de>, 2016-2017, 2021.

<p>Debian wird auf hunderte von Servern im Internet verteilt 
   (<em>gespiegelt</em>). Durch die Verwendung eines Servers in Ihrer Nähe
   wird wahrscheinlich das Herunterladen beschleunigt und auch die Last auf
   den zentralen Servern und das Internet im Gesamten reduziert.</p>

<p class="centerblock">
   Debian-Spiegel gibt es in vielen Ländern, und für einige haben wir
   Alias-Namen der Art <code>ftp.&lt;land&gt;.debian.org</code> eingerichtet.
   Dieser Alias verweist normalerweise auf einen Spiegel, der regelmäßig und
   schnell synchronisiert wird und alle Debian-Architekturen enthält. Das
   Debian-Archiv ist über <code>HTTP</code> immer unter <code>/debian</code>
   auf dem Server zu finden.
</p>

<p class="centerblock">
   Andere <strong>Spiegel-Server</strong> können (aufgrund von Platzbeschränkungen)
   gewissen Einschränkungen unterliegen, was gespiegelt wird. Lediglich die Tatsache,
   dass ein Server nicht der <code>ftp.&lt;land&gt;.debian.org</code>-Spiegel
   eines Landes ist, bedeutet nicht, dass ein solcher langsamer oder
   weniger aktuell ist als der <code>ftp.&lt;land&gt;.debian.org</code>-Spiegel.
   Im Gegenteil: ein Spiegel, der die von Ihnen benötigte Architektur enthält und
   in Ihrer Nähe liegt (und aufgrunddessen für Sie schneller ist), ist anderen
   Spiegeln, die weiter entfernt liegen, fast immer vorzuziehen.
</p>

<p>Verwenden Sie den Server, der Ihnen am nächsten liegt, um die höchste
   Geschwindigkeit zu erreichen, egal ob es der Alias-Spiegel Ihres Landes ist
   oder nicht. Das
   Programm <a href="https://packages.debian.org/stable/net/netselect">\
   <em>Netselect</em></a> kann zur Bestimmung der Site mit der geringsten 
   Latenzzeit bestimmt werden; verwenden Sie Programme zum Herunterladen wie
   <a href="https://packages.debian.org/stable/web/wget"><em>Wget</em></a> oder
   <a href="https://packages.debian.org/stable/net/rsync"><em>Rsync</em></a> zur
   Bestimmung der Site mit dem größten Durchsatz. Beachten Sie, dass 
   geographische Nähe oft kein wichtiger Faktor bei der Wahl der für Sie am 
   besten geeigneten Maschine ist.</p>

<p>
Wenn Sie mit Ihrem Rechner viel unterwegs sind, sind Sie mit einem
<q>Spiegel</q>, der ein globales <abbr title="Content Delivery Network">CDN</abbr>
im Rücken hat, vielleicht am besten bedient. Das Debian-Projekt betreibt
für diesen Zweck <code>deb.debian.org</code> und Sie können dies in Ihrer
apt-sources.list verwenden &ndash; besuchen Sie dessen 
<a href="http://deb.debian.org/">Website</a> bezüglich weiterer Details.
</p>

<p>Die maßgebliche Kopie der folgenden Liste ist stets unter dieser URL
   zu finden: <url "https://www.debian.org/mirror/list">.
   Alles, was Sie sonst über Debian-Spiegel wissen möchten:
   <url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">Debian-Spiegel-Alias nach Ländern</h2>

<table border="0" class="center">
<tr>
  <th>Land</th>
  <th>Site</th>
  <th>Architekturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Liste der Spiegel des Debian-Archivs</h2>

<table border="0" class="center">
<tr>
  <th>Rechnername</th>
  <th>HTTP</th>
  <th>Architekturen</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
