#use wml::debian::template title="Debian am Desktop"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896"
# $Id$
# Translation: Gerfried Fuchs <alfie@debian.org> 2002-10-24
# Translation update: Holger Wansing <hwansing@mailbox.org>, 2019.

<h2>Das universelle Betriebssystem als Desktop</h2>

<p>
  Das Debian-Desktop Unterprojekt ist eine Gruppe von Freiwilligen, die das
  bestmögliche Betriebssystem für Privat- und Firmen-Arbeitsplätze erstellen
  möchten. Unser Motto ist <q>Software, die einfach funktioniert</q>.
  Kurz gesagt ist es unser Ziel, Debian, GNU und Linux in die
  Welt der durchschnittlichen Benutzer zu bringen.
  <img style="float: right;" src="debian-desktop.png" alt="Debian-Desktop" />
</p>


<h3>Unsere Lehre</h3>

<ul>
  <li>
    Wir sehen, dass viele verschiedene
    <a href="https://wiki.debian.org/DesktopEnvironment">Desktop-Umgebungen</a>
    existieren, und wir werden deren Anwendung unterstützen sowie sicherstellen,
    dass sie in Debian gut funktionieren.
  </li>
  <li>
    Wir erkennen, dass es nur zwei wichtige Klassen von Benutzern gibt: Die
    Anfänger und die Experten. Wir werden alles tun, um die Dinge für die
    Anfänger sehr einfach zu machen, während wir es den Experten erlauben,
    Dinge zu verändern, falls sie wollen.
  </li>
  <li>
    Wir werden darauf hinarbeiten, dass Software für die gebräuchlichste
    Desktop-Verwendung konfiguriert ist. Zum Beispiel sollte das reguläre
    Benutzer-Konto, das während der Installation standardmäßig hinzugefügt
    wird, die Berechtigung haben, Audio und Video abzuspielen, zu
    Drucken und das System über sudo zu verwalten.
  </li>
  <li>
    <p>Wir werden daran arbeiten, dass Fragen, die dem Benutzer gestellt
    werden (die auf einem Minimum gehalten werden sollen), selbst mit einem
    Minimum an Computerkenntnissen Sinn ergeben. Einige Debian-Pakete
    überfordern den Benutzer mit schwierigen technischen Details. 
    Für den Anfänger ist das verwirrend und erschreckend.
    Für den Experten ist dies nervend und unnötig. 
    Ein Anfänger weiß vielleicht nicht mal, worum es in den Fragen geht.
    Ein Experte kann seine Desktop-Umgebung passend konfigurieren, so dass er
    die Fragen nicht mehr zu sehen bekommt, andererseits möchte er die
    Anzeigefunktionalität nach Abschluß der Installation eventuell durchaus haben.
    Die Priorität dieser Art von debconf-Fragen
    sollten zumindest heruntergeschraubt werden.</p>
  </li>
  <li>
    Und wir werden Spaß daran haben, das alles zu tun!
  </li>
</ul>


<h3>Wie können Sie helfen</h3>

<p>
  Die wichtigsten Teile eines Debian-Unterprojekts sind nicht Mailinglisten,
  Webseiten, oder Archiv-Platz für Pakete. Der wichtigste Teil sind
  <em>motivierte Leute</em>, die die Dinge umsetzen. Sie müssen kein
  offizieller Entwickler sein, um mit dem Erstellen von Paketen und Patches
  zu beginnen. Das zentrale Debian-Desktop-Team wird sicherstellen, dass
  Ihre Arbeit integriert wird. Hier also einige Dinge, die Sie tun können:
</p>

<ul>
   <li>
     Testen sie unsere Programmgruppe der <q>Standard-Desktop-Umgebung</q> (oder die
     Programmgruppe kde-desktop), auszuwählen während der Installation; 
     installieren sie eines unserer <a 
     href="$(DEVEL)/debian-installer/">Images für die nächste Testing-Veröffentlichung</a>
     und senden Sie Rückmeldungen an die <a
     href="https://lists.debian.org/debian-desktop/">debian-desktop 
     Mailingliste</a>.
  </li>
  <li>
     Arbeiten Sie am <a href="$(devel)/debian-installer/">Debian-Installer</a>.
     Die gtk+-Oberfläche braucht Sie!
  </li>
  <li>
     Helfen Sie dem <a href="https://wiki.debian.org/Teams/DebianGnome">Debian-GNOME-Team</a>,
     dem <a href="https://qt-kde-team.pages.debian.net/">Debian-Qt- und KDE-Team</a>
     oder der <a href="https://salsa.debian.org/xfce-team/">Debian-Xfce-Gruppe</a>.
     Sie können bei Paketierungsarbeiten, dem Verwalten von Fehlerberichten, bei Dokumentation,
     Test und weiterem helfen.
  </li>
  <li>
     Bringen Sie Benutzern bei, wie die Debian-Desktop-Programmgruppen, die wir derzeit haben,
     installiert und verwendet werden (desktop, gnome-desktop und kde-desktop).
  </li>
  <li>
    Arbeiten Sie daran, die Priorität von unnötigen
    <a href="https://packages.debian.org/debconf">debconf</a> Fragen
    herunterzuschrauben bzw. sie aus
    Paketen zu entfernen, und machen Sie die notwendigen einfach verständlich.
  </li>
  <li>
     Helfen Sie mit bei <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian
     Desktop Artwork</a>, dem Erstellen künstlerischer Arbeiten für Debian.
  </li>
</ul>


<h3>Wiki</h3>

<p>
  Wir haben einige Artikel in unserem Wiki, und unsere Startseite ist: <a 
  href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>. Einige Debian
  Desktop-Wiki-Artikel sind veraltet.
</p>

<h3>Mailingliste</h3>
<p>
   Dieses Unterprojekt wird auf der <a 
   href="https://lists.debian.org/debian-desktop/">debian-desktop</a> 
   Mailingliste diskutiert.
 </p>


<h3>IRC-Kanal</h3>

<p>
  Wir ermutigen jeden (Debian-Entwickler oder auch nicht), der an
  Debian-Desktop interessiert ist, #debian-desktop im
  <a href="http://oftc.net/">OFTC-IRC</a> (irc.debian.org) zu
  besuchen.
</p>


<h3>Wer ist beteiligt?</h3>

<p>
  Jeder der möchte, ist willkommen. Tatsächlich ist jeder in der 
  pkg-gnome-, pkg-kde- oder pkg-xfce-Gruppe indirekt beteiligt. Die 
  debian-desktop Mailinglisten-Abonnenten sind aktive Beitragende. Die
  Debian-Installer- und Tasksel-Gruppen sind für unsere Ziele ebenfalls wichtig.
</p>


<p>
  Diese Webseite wird von <a href="https://people.debian.org/~stratus/">Gustavo 
  Franco</a> betreut. Frühere Betreuer waren <a 
  href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> und
 <a href="https://people.debian.org/~walters/">Colin Walters</a>.
</p>
