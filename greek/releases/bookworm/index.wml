#use wml::debian::template title="Πληροφορίες κυκλοφορίας της έκδοσης Debian &ldquo;bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="43ed34ae31e2b37b42af378b2dcc661486ce5646" maintainer="galaxico"

<if-stable-release release="bookworm">

<p>Η έκδοση Debian <current_release_bookworm> κυκλοφόρησε
στις <a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 was initially released on <:=spokendate('XXXXXXXX'):>."
/>
Η έκδοση περιείχε αρκετές μείζονες αλλαγές,
που περιγράφονται στη σελίδα μας
<a href="$(HOME)/News/XXXX/XXXXXXXX">Δελτίο τύπου</a> και
στις <a href="releasenotes">Σημειώσεις της Έκδοσης</a>.</p>

#<p><strong>Debian 12 has been superseded by
#<a href="../trixie/">Debian 13 (<q>trixie</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, bookworm benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in bookworm.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>To obtain and install Debian, see
the <a href="debian-installer/">installation information</a> page and the
<a href="installmanual">Installation Guide</a>. To upgrade from an older
Debian release, see the instructions in the
<a href="releasenotes">Release Notes</a>.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Computer architectures supported at initial release of bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Contrary to our wishes, there may be some problems that exist in the
release, even though it is declared <em>stable</em>. We've made
<a href="errata">a list of the major known problems</a>, and you can always
<a href="reportingbugs">report other issues</a> to us.</p>

<p>Last but not least, we have a list of <a href="credits">people who take
credit</a> for making this release happen.</p>
</if-stable-release>

<if-stable-release release="bullseye">

<p>Το κωδικό όνομα της επόμενης μείζονος κυκλοφορίας του Debian μετά την έκδοση <a
href="../bullseye /">bullseye</a> είναι <q>bookworm</q>.</p>

<p>Αυτή η έκδοση ξεκίνησε ως μια κόπια της έκδοσης bullseye, και είναι αυτή τη στιγμή σε μια κατάσταση
που λέγεται <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">δοκιμαστική</a></q>.
Αυτό σημαίνει ότι πράγματα δεν "χαλάνε" τόσο άσχημα όσο στην ασταθή ή την πειραματική διανομή, 
επειδή τα πακέτα μπορούν να μπουν σ' αυτήν μόνο αφού έχει περάσει ένα χρονικό διάστημα
και όταν δεν έχουν αναφερθεί γι' αυτά οποιαδήποτε κρίσιμα για την κυκλοφορία της διανομής σφάλματα.</p>

<p>Παρακαλούμε σημειώστε ότι τις επικαιροποιήσεις για την <q>δοκιμαστική</q> διανομή δεν τις διαχειρίζεται ακόμα η ομάδα ασφαλείας. Συνεπώς, η <q>δοκιμαστική</q> διανομή <strong>δεν</strong> λαμβάνει έγκαιρα επικαιροποιήσεις ασφαλείας.

# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.

Σας ενθαρρύνουμε να αλλάξετε, προς τα παρόν, τις σχετικές εισόδους στο αρχείο sources.list από την δοκιμαστική στην bullseye 
αν χρειάζεστε υποστήριξη ασφαλείας. Δείτε επίσης τα σχετικά στη σελίδα
<a href="$(HOME)/security/faq#δοκιμαστική">Συχνές Ερωτήσεις της Ομάδας Ασφαλείας</a> για την <q>δοκιμαστική</q> διανομή.</p>


<p>Ίσως υπάρχει διαθέσιμο ένα <a href="releasenotes">προσχέδιο των Σημειώσεων της έκδοσης</a>.
Παρακαλούμε επίσης <a href="https://bugs.debian.org/release-notes">ελέγξτε τις
προτεινόμενες προσθήκες στις σημειώσεις της έκδοσης</a>.</p>

<p>Για εικόνες εγκατάστασης και τεκμηρίωση σχετικά με το πώς να εγκαταστήσετε τη <q>δοκιμαστική έκδοση</q>,
δείτε τη σελίδα του <a href="$(HOME)/devel/debian-installer/">Εγκαταστάστη του Debian</a>.</p>

<p>Για να βρείτε περισσότερα σχετικά με το πώς δουλεύει η <q>δοκιμαστική</q> διανομή, ελέγξτε τις 
<a href="$(HOME)/devel/testing">πληροφορίες των προγραμματιστών/τριών σχετικά με αυτό</a>.</p>

<p>Ο κόσμος ρωτάει συχνά αν υπάρχει ένας μοναδικός <q>μετρητής προόδου</q> της έκδοσης. 
Δυστυχώς δεν υπάρχει κάτι τέτοιο, αλλά μπορούμε να σας παραπέμψουμε σε αρκετά σημεία
που περιγράφουν πράγματα που πρέπει να αντιμετωπιστούν ώστε να γίνει η κυκλοφορία της έκδοσης:</p>



<ul>
  <li><a href="https://release.debian.org/">Σελίδα της γενικής κατάστασης της έκδοσης</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Σφάλματα κρίσιμα για την κυκλοφορία της έκδοσης</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Σφάλματα του βασικού συστήματος</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Σφάλματα στα βασικά πακέτα και τα πακέτα καθηκόντων (tasks)</a></li>
</ul>


<p>Επιπλέον, αναφορές για τη γενική κατάσταση της έκδοσης αναρτώνται από τον
διαχειριστή της κυκλοφορίας της έκδοσης στην λίστα αλληλογραφίας 
<a href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce</a>.</p>


</if-stable-release>

<if-stable-release release="buster">

<p>The code name for the next major Debian release after <a
href="../bullseye /">bullseye</a> is <q>bookworm</q>. Currently,
<q>bullseye</q> is not released yet. So <q>bookworm</q> is still
far away.</p>

</if-stable-release>
