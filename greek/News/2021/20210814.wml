#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="galaxico"
<define-tag pagetitle>Debian 11 <q>bullseye</q> released</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news


<p>After 2 years, 1 month, and 9 days of development, the Debian 
project is proud to present its new stable version 11 (code name <q>bullseye</q>),
which will be supported for the next 5 years thanks to the combined work of the
<a href="https://security-team.debian.org/">Debian Security team</a> 
and the <a href="https://wiki.debian.org/LTS">Debian Long Term Support</a> team.
</p>

<p>
Debian 11 <q>bullseye</q> ships with several desktop applications and
environments. Amongst others it now includes the desktop environments:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>This release contains over 11,294 new packages for a total count of 59,551
packages, along with a significant reduction of over 9,519 packages which were marked as
<q>obsolete</q> and removed. 42,821 packages were updated and 5,434 packages remained
unchanged.
</p>

<p>
<q>bullseye</q>  becomes our first release to provide a Linux kernel with support for the
exFAT filesystem and defaults to using it for mount exFAT filesystems.
Consequently it is no longer required to use the filesystem-in-userspace
implementation provided via the exfat-fuse package. Tools for creating
and checking an exFAT filesystem are provided in the exfatprogs package.
</p>


<p>
Most modern printers are able to use driverless printing and scanning without
the need for vendor specific (often non-free) drivers. 

<q>bullseye</q> brings forward a new package, ipp-usb, which uses the vendor neutral
IPP-over-USB protocol supported by many modern printers. This allows a USB
device to be treated as a network device. The official SANE driverless backend
is provided by sane-escl in libsane1, which uses the eSCL protocol.
</p>

<p>
Systemd in <q>bullseye</q> activates its persistent journal functionality, by default,
with an implicit fallback to volatile storage. This allows users that are not
relying on special features to uninstall traditional logging daemons and
switch over to using only the systemd journal.
</p>

<p>
The Debian Med team has been taking part in the fight against COVID-19 
by packaging software for researching the virus on the sequence level 
and for fighting the pandemic with the tools used in epidemiology; this work 
will continue with focus on machine learning tools for both fields. The team's
work with Quality Assurance and Continuous integration is critical to the 
consistent reproducible results required in the sciences. 

Debian Med Blend has a range of performance critical applications which now
benefit from SIMD Everywhere. To install packages maintained by the Debian Med
team, install the metapackages named med-*, which are at version 3.6.x. 
</p>

<p>
Chinese, Japanese, Korean, and many other languages now have a new Fcitx 5 input
method, which is the successor of the popular Fcitx4 in <q>buster</q>; this new version
has much better Wayland (default display manager) addon support.
</p>

<p>
Debian 11 <q>bullseye</q> includes numerous updated software packages (over
72% of all packages in the previous release), such as:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 series</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>more than 59,000 other ready-to-use software packages, built from
more than 30,000 source packages.</li>
</ul>

<p>
With this broad selection of packages and its traditional wide
architecture support, Debian once again stays true to its goal of being
<q>The Universal Operating System</q>. It is suitable for many different use
cases: from desktop systems to netbooks; from development servers to
cluster systems; and for database, web, and storage servers. At the same
time, additional quality assurance efforts like automatic installation
and upgrade tests for all packages in Debian's archive ensure that
<q>bullseye</q> fulfills the high expectations that users have of a
stable Debian release.
</p>

<p>
A total of nine architectures are supported:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
for ARM, <code>armel</code>
and <code>armhf</code> for older and more recent 32-bit hardware,
plus <code>arm64</code> for the 64-bit <q>AArch64</q> architecture,
and for MIPS, <code>mipsel</code> (little-endian) architectures for 32-bit hardware
and <code>mips64el</code> architecture for 64-bit little-endian hardware.
</p>

<h3>Want to give it a try?</h3>
<p>
If you simply want to try Debian 11 <q>bullseye</q> without installing it,
you can use one of the available <a href="$(HOME)/CD/live/">live images</a> which load and run the
complete operating system in a read-only state via your computer's memory.
</p>

<p>
These live images are provided for the <code>amd64</code> and
<code>i386</code> architectures and are available for DVDs, USB sticks,
and netboot setups. The user can choose among different desktop
environments to try: GNOME, KDE Plasma, LXDE, LXQt, MATE, and Xfce.
Debian Live <q>bullseye</q> has a standard live image, so it is also possible
to try a base Debian system without any of the graphical user interfaces.
</p>

<p>
Should you enjoy the operating system you have the option of installing
from the live image onto your computer's hard disk. The live image
includes the Calamares independent installer as well as the standard Debian Installer.
More information is available in the
<a href="$(HOME)/releases/bullseye/releasenotes">release notes</a> and the
<a href="$(HOME)/CD/live/">live install images</a> sections of
the Debian website.
</p>

<p>
To install Debian 11 <q>bullseye</q> directly onto your
computer's hard disk you can choose from a variety of installation media
such as Blu-ray Disc, DVD, CD, USB stick, or via a network connection.
Several desktop environments &mdash; Cinnamon, GNOME, KDE Plasma Desktop and
Applications, LXDE, LXQt, MATE and Xfce &mdash; may be installed through those
images.
In addition, <q>multi-architecture</q> CDs are available which support
installation from a choice of architectures from a single disc. Or you can
always create bootable USB installation media
(see the <a href="$(HOME)/releases/bullseye/installmanual">Installation Guide</a>
for more details).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>
There has been a lot of development on the Debian Installer,
resulting in improved hardware support and other new features.
</p>
<p>
In some cases, a successful installation can still have display issues
when rebooting into the installed system; for those cases there are
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">a few workarounds</a>
that might help log in anyway.
There is also an
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">isenkram-based procedure</a>
which lets users detect and fix missing firmware on their systems,
in an automated fashion. Of course, one has to weigh the pros and
cons of using that tool since it's very likely that it will need
to install non-free packages.</p>

<p>
  In addition to this, the
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">non-free installer images that include firmware packages</a>
  have been improved so that they can anticipate the need for firmware
  in the installed system (e.g. firmware for AMD or Nvidia graphics
  cards, or newer generations of Intel audio hardware).
</p>

<p>
For cloud users, Debian offers direct support for many of the
best-known cloud platforms. Official Debian images are easily
selected through each image marketplace. Debian also publishes <a
href="https://cloud.debian.org/images/openstack/current/">pre-built
OpenStack images</a> for the <code>amd64</code> and <code>arm64</code>
architectures, ready to download and use in local cloud setups.
</p>

<p>
Debian can now be installed in 76 languages, with most of them available
in both text-based and graphical user interfaces.
</p>

<p>
The installation images may be downloaded right now via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (the recommended method),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, or
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; see
<a href="$(HOME)/CD/">Debian on CDs</a> for further information. <q>bullseye</q> will
soon be available on physical DVD, CD-ROM, and Blu-ray Discs from
numerous <a href="$(HOME)/CD/vendors">vendors</a> too.
</p>


<h3>Upgrading Debian</h3>
<p>
Upgrades to Debian 11 from the previous release, Debian 10
(code name <q>buster</q>) are automatically handled by the APT
package management tool for most configurations.
</p>

<p>
For bullseye, the security suite is now named bullseye-security 
and users should adapt their APT source-list files accordingly when upgrading.
If your APT configuration also involves pinning or <code>APT::Default-Release</code>, 
it is likely to require adjustments too. See the
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">Changed security archive layout</a>
section of the release notes for more details.
</p>

<p>
If you are upgrading remotely, be aware of the section 
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">No new SSH connections possible during upgrade</a>. 
</p>

<p>
As always, Debian systems may be upgraded painlessly, in place,
without any forced downtime, but it is strongly recommended to read
the <a href="$(HOME)/releases/bullseye/releasenotes">release notes</a> as
well as the <a href="$(HOME)/releases/bullseye/installmanual">installation
guide</a> for possible issues, and for detailed instructions on
installing and upgrading. The release notes will be further improved and
translated to additional languages in the weeks after the release.
</p>


<h2>About Debian</h2>

<p>
Debian is a free operating system, developed by
thousands of volunteers from all over the world who collaborate via the
Internet. The Debian project's key strengths are its volunteer base, its
dedication to the Debian Social Contract and Free Software, and its
commitment to provide the best operating system possible. This new
release is another important step in that direction.
</p>


<h2>Contact Information</h2>

<p>
For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.
</p>
