#use wml::debian::template title="Derivados do Debian"
#use wml::debian::translation-check translation="1b62dcf348569f7b3d0975c674a776e714dc31fc"

<p>
Existem <a href="#list">várias distribuições</a> baseadas no Debian.
Algumas pessoas podem querer dar uma olhada nessas distribuições
<em>além</em> dos lançamentos oficiais do Debian.
</p>

<p>
Um derivado do Debian é uma distribuição baseada no trabalho realizado no
Debian, mas com sua própria identidade, objetivos e público-alvo, e é criado
por uma entidade que é independente do Debian.
Os derivados modificam o Debian para atingir os objetivos que eles mesmos
estabeleceram.
</p>

<p>
O Debian dá boas-vindas e encoraja organizações que desejam desenvolver novas
distribuições baseadas no Debian.
No espírito do <a href="$(HOME)/social_contract">contrato social</a> do Debian,
esperamos que os derivados contribuam com seu trabalho para o Debian e os
projetos dos(as) autores(as) originais, para que todos(as) possam se beneficiar
de suas melhorias.
</p>

<h2 id="list">Quais derivados estão disponíveis?</h2>

<p>
Gostaríamos de destacar os seguintes derivados do Debian:
</p>

## Por favor, mantenha esta lista em ordem alfabética
## Adicione apenas derivados que atendam aos critérios abaixo
<ul>
    <li>
      <a href="https://grml.org/">Grml</a>:
      sistema live para administradores(as) de sistema.
      <a href="https://wiki.debian.org/Derivatives/Census/Grml">Mais informações</a>.
    </li>
    <li>
      <a href="https://www.kali.org/">Kali Linux</a>:
      auditoria de segurança e testes de invasão.
      <a href="https://wiki.debian.org/Derivatives/Census/Kali">Mais informações</a>.
    </li>
    <li>
      <a href="https://pureos.net/">Purism PureOS</a>:
      <a href="https://www.fsf.org/news/fsf-adds-pureos-to-list-of-endorsed-gnu-linux-distributions-1">endossado pela FSF</a>
      lançamento contínuo (rolling release) focado em privacidade,
      segurança e conveniência.
      <a href="https://wiki.debian.org/Derivatives/Census/Purism">Mais informações</a>.
    </li>
    <li>
      <a href="https://tails.boum.org/">Tails</a>:
      preservação da privacidade e anonimato.
      <a href="https://wiki.debian.org/Derivatives/Census/Tails">Mais informações</a>.
    </li>
    <li>
      <a href="https://www.ubuntu.com/">Ubuntu</a>:
      popularização do Linux em todo o mundo.
      <a href="https://wiki.debian.org/Derivatives/Census/Ubuntu">Mais informações</a>.
    </li>
</ul>

<p>
Além disso, as distribuições baseadas no Debian estão listadas no
<a href="https://wiki.debian.org/Derivatives/Census">censo de derivados do Debian</a>
assim como em <a href="https://wiki.debian.org/Derivatives#Lists">outros lugares</a>.
</p>

<h2>Por que usar um derivado em vez do Debian?</h2>

<p>
Se você tem uma necessidade específica que é melhor atendida por um derivado,
você pode preferir usá-lo ao invés do Debian.
</p>

<p>
Se você faz parte de uma comunidade específica ou grupo de pessoas e existe
um derivado para esse grupo, você pode preferir usá-lo em vez do Debian.
</p>

<h2>Por que o Debian está interessado em derivados?</h2>

<p>
Os derivados levam o Debian a um número maior de pessoas com experiências e
necessidades mais diversas do que o público que alcançamos atualmente.

Ao desenvolvermos relacionamentos com os derivados,
<a href="https://wiki.debian.org/Derivatives/Integration">integrando</a>
informações sobre eles na infraestrutura Debian e mesclando as mudanças que
eles fazem de volta no Debian, compartilhamos nossa experiência com nossos
derivados, expandimos nosso entendimento de nossos derivados e seus públicos,
potencialmente expandimos a comunidade Debian, melhoramos o Debian para o
nosso público existente e tornamos o Debian adequado para um público mais
diversificado.
</p>

<h2>Quais derivados o Debian destaca?</h2>

## Exemplos desses critérios estão no arquivo README.txt que o acompanha
<p>
Os derivados destacados acima atenderam à maioria destes critérios:
</p>

<ul>
    <li>cooperação ativa com o Debian.</li>
    <li>são mantidos ativamente.</li>
    <li>tem uma equipe de pessoas envolvidas, incluindo pelo menos um(a) membro(a) do Debian.</li>
    <li>ingressou no censo dos derivados do Debian e incluiu um arquivo sources.list em sua página de censo.</li>
    <li>tem um recurso distinto ou foco.</li>
    <li>são distribuições notáveis e estabelecidas.</li>
</ul>

<h2>Por que fazer um derivado do Debian?</h2>

<p>
Pode ser mais rápido modificar uma distribuição existente como o Debian do que
começar do zero, já que um formato de empacotamento, repositórios, pacotes-base
e outras coisas estão especificadas e usáveis.
Um monte de software está empacotado, portanto não há necessidade de gastar
tempo empacotando a maioria das coisas.
Isso permite que os derivados se concentrem nas necessidades de um público
específico.
</p>

<p>
O Debian garante que o que distribuímos seja
<a href="$(HOME)/intro/free">livre</a> para que os derivados modifiquem e
redistribuam para o seu público.
Fazemos isso comparando as licenças dos softwares que distribuímos com a
<a href="$(HOME)/social_contract#guidelines">Definição Debian de Software Livre (DFSG)</a>.
</p>

<p>
O Debian tem vários ciclos diferentes de
<a href="$(HOME)/releases/">lançamento</a> disponíveis para os derivados
basearem suas distribuições.

Isso permite que os derivados
testem softwares <a href="https://wiki.debian.org/DebianExperimental">experimentais</a>,
movimentem-se realmente <a href="$(HOME)/releases/unstable/">rápido</a>,
atualizem-se <a href="$(HOME)/releases/testing/">frequentemente</a> com garantia
de qualidade,
tenham uma <a href="$(HOME)/releases/stable/">base sólida</a> para seus trabalhos,
usem softwares <a href="https://backports.debian.org/">mais recentes</a> sobre
uma base sólida,
aproveitem o suporte <a href="$(HOME)/security/">de segurança</a> e
<a href="https://wiki.debian.org/LTS">estendam</a> esse suporte.
</p>

<p>
O Debian suporta várias <a href="$(HOME)/ports/">arquiteturas</a> diferentes
e os(as) contribuidores(as) estão
<a href="https://wiki.debian.org/DebianBootstrap">trabalhando</a> em métodos
para criação automática de novos suportes para novos tipos de processadores.
Isso permite que os derivados usem o hardware de sua escolha ou suportem novos
projetos de processadores.
</p>

<p>
A comunidade Debian e as pessoas dos derivados existentes estão disponíveis e
dispostas a ajudar a orientar novas distribuições em seus trabalhos.
</p>

<p>
Os derivados são criados por vários motivos, como
tradução para novos idiomas,
suporte a um hardware específico,
diferentes mecanismos de instalação ou
suporte a uma comunidade específica ou grupo de pessoas.
</p>

<h2>Como fazer um derivado do Debian?</h2>

<p>
Os derivados podem usar partes da infraestrutura do Debian, se necessário
(como repositórios).
Os derivados devem alterar as referências ao Debian (como o logotipo, nome,
etc.) e aos serviços Debian (como o site web e o BTS).
</p>

<p>
Se o objetivo é definir um conjunto de pacotes a serem instalados, a criação de
um <a href="$(HOME)/blends/">Debian blend</a> pode ser uma maneira interessante
de fazer isso no Debian.
</p>

<p>
Informações detalhadas sobre desenvolvimento estão disponíveis nas
<a href="https://wiki.debian.org/Derivatives/Guidelines">diretrizes</a> e a
orientação está disponível a partir do
<a href="https://wiki.debian.org/DerivativesFrontDesk">front desk</a>.
</p>
