#use wml::debian::template title="Grandes eventos relacionados ao Debian" BARETITLE=true
#use wml::debian::translation-check translation="7ff6f5b6db32d0aa1bc33e38c8014c6a83685f42"

<p>O maior evento do Debian é a Conferência Debian anual chamada de
<a href="https://www.debconf.org/">DebConf</a>. Também ocorrem diversas
<a href="https://wiki.debian.org/MiniDebConf">MiniDebConfs</a> que são
reuniões locais organizadas por membros(as) do projeto Debian.
</p>

<p>As subpáginas da <a href="https://wiki.debian.org/DebianEvents">página wiki
de eventos do Debian</a> contêm uma lista de eventos nos quais o Debian
esteve, está ou estará envolvido.</p>

<p>Se você quiser ajudar na apresentação do Debian em algum dos eventos listados
nas páginas acima, por favor, envie um e-mail para a pessoa apontada como
coordenador(a) na respectiva página do evento.</p>

<p>Se você gostaria de convidar o Projeto Debian para um novo evento, por favor,
envie um e-mail em inglês para a
<a href="eventsmailinglists">lista de discussão da localização geográfica</a>
do evento.</p>

<p>Para eventos no Brasil, por favor, envie um e-mail para a
<a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos">lista de discussão debian-br-eventos</a></p>

<p>Caso você queira ajudar a coordenar a presença do Debian em um evento,
por favor, verifique a subpágina com a melhor opção na
<a href="https://wiki.debian.org/DebianEvents">página wiki de eventos do Debian</a>.
Se já houver uma entrada para o evento em questão, contribua ali - em caso
negativo, siga a <a href="checklist">Checklist para organizar um estande</a>.</p>

<p>Se você quiser saber quais materiais de propaganda (merchandise) outras
empresas estão criando com o logotipo do Debian, o qual, entre outras
situações, pode ser usado em eventos, verifique nossa
<a href="merchandise">página de merchandise</a>.</p>

<p>Se planeja fazer uma apresentação, você deve criar uma entrada no fim da
<a href="https://wiki.debian.org/DebianEvents">página wiki de eventos do Debian</a>.
Após realizada sua atividade, você poderia enviar a apresentação para
<a href="https://wiki.debian.org/Presentations">a coleção de apresentações sobre Debian</a>.</p>

## Isto também deveria ser limpo.
<define-tag event_year>Eventos em %d</define-tag>
<define-tag past_words>eventos passados</define-tag>
<define-tag none_word>nenhum</define-tag>

#include "$(ENGLISHDIR)/events/index.include"

## <h3>Próximos Eventos com participação do Debian</h3>

<:= get_future_event_list(); :>

## <p>A lista acima geralmente está incompleta e ultrapassada em relação à
## <a href="https://wiki.debian.org/DebianEvents">página wiki de eventos do
## Debian</a>.</p>

<h3>Várias páginas úteis</h3>

<ul>
  <li> <a href="checklist">Checklist</a> para um estande</li>
  <li> <a href="https://wiki.debian.org/DebianEvents">Página wiki de eventos do
    Debian</a></li>
  <li> <a href="material">Materiais e Merchandising</a> para um estande</li>
  <li> <a href="keysigning">Assinatura de chaves</a></li>
  <li> <a href="https://wiki.debian.org/Presentations">Apresentações sobre o
    Debian</a></li>
  <li> <a href="admin">Trabalhos</a> nos events@debian.org</li>
</ul>

<h3>Eventos Passados</h3>

<p>Eventos anteriores à transição para o
<a href="https://wiki.debian.org/DebianEvents">wiki</a> podem ser vistos através
das seguintes páginas:

<:= get_past_event_list(); :>
