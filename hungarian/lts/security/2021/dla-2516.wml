#use wml::debian::translation-check translation="4139a249d5e4ddb427ae3c7ef8c33038092b7a8b" maintainer="Szabolcs Siebenhofer"
<define-tag description>LTS biztonsági frissítés</define-tag>
<define-tag moreinfo>

<p>Problémát fedeztek fel a gssproxy jogosultság megosztásában, amit az okoz,
hogy a <em>gssproxy</em> által nem kerül felszabadításra a <pre>cond_mutex</pre>,
mielőtt meghívná a <pre>pthread_exit</pre>-et.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12658">CVE-2020-12658</a>

    <p>gssproxy (aka gss-proxy) a 0.8.3 előtt nem szabadítja fel a cond_mutex-et 
    mielőtt a ptherad kilép a gp_workers.c-ben lévő gp_worker_main()-ből.</p></li>

</ul>

<p>A Debian 9 <q>stretch</q> esetén a probléma a 0.5.1-2+deb9u1  verzióban javításra 
került.</p>

<p>Azt tanácsoljuk, hogy frissítsd a gssproxy csomagjaidat.</p>

<p>További információk a Debian LTS biztonági figyelmeztetéseiről, hogyan tudod ezeket a 
frissítéseket a rendszereden telepíteni és más gyakran feltett kérdések megtalálhatóak itt: 
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2516.data"
# $Id: $
