#use wml::debian::template title="Merci de télécharger Debian !"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="dce6879deecfce73e2c294a9b48d9d70677fddca" maintainer="Jean-Paul Guillonneau"



<div id="content">
<meta http-equiv="refresh" content="3;url=<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
</div>

<p>Il s’agit de la version <em>netinst</em> pour Debian <:=substr '<current_initial_release>', 0, 2:>,
nom de code <em><current_release_name></em> pour <: print $arches{'amd64'}; :>.</p>

<p>Si le téléchargement ne démarre pas automatiquement, cliquez sur
<a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso</a>.</p>
<p>Téléchargement de la somme de contrôle :
<a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS">SHA512SUMS</a>
<a href="<stable-images-url/>/amd64/iso-cd/SHA512SUMS.sign">Signature</a></p>



<div class="tip">
	<p><strong>Important</strong> :
<a href="$(HOME)/CD/verify">Veiller à vérifier la somme de contrôle</a>.</p>
</div>

<p>Les ISO de l’installateur sont des images hybrides, ce qui signifie qu’elles
peuvent être écrites directement sur des CD, DVD ou BD, ou sur des
<a href="https://www.debian.org/CD/faq/#write-usb">clés USB</a>.</p>

<h2 id="h2-1">
	Autres installateurs</h2>

<p>Les autres images et installateurs, tels que les systèmes autonomes, les
installateurs pour des systèmes sans connexion réseau, les installateurs pour
d’autres architectures ou les instances d’informatique dématérialisée, peuvent
être trouvés sur la page <a href="$(HOME)/distrib/">Obtenir Debian</a>.</p>

<p>Les installateurs non officiels avec du
<a href="https://wiki.debian.org/Firmware"><strong>microcode non libre</strong></a>,
utiles pour quelques adaptateurs réseau et vidéo, peuvent être téléchargés
à partir de la page
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">Images
non officielles incluant des paquets de microcode</a> (en anglais).</p>

<h2 id="h2-2">Liens utiles</h2>

<p><a href="$(HOME)/releases/<current_release_name>/installmanual">Manuel d'installation</a></p>
<p><a href="$(HOME)/releases/<current_release_name>/releasenotes">Notes de publication</a></p>
<p><a href="$(HOME)/CD/verify">Vérification de l'authenticité des CD Debian</a></p>
<p><a href="$(HOME)/CD/http-ftp/#mirrors">Miroirs enregistrés de l'archive debian-cd</a></p>
<p><a href="$(HOME)/releases">Les versions de Debian</a></p>


