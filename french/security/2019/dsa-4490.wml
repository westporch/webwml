#use wml::debian::translation-check translation="bbdc5fdd9cf31c8e006e51f38f90961f3cb78f42" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Subversion, un système
de gestion de versions. Le projet « Common Vulnerabilities and Exposures »
(CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11782">CVE-2018-11782</a>

<p>Ace Olszowka a signalé que le processus du serveur svnserve de
Subversion peut se terminer quand une requête en lecture seule bien formée
produit une réponse particulière, menant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0203">CVE-2019-0203</a>

<p>Tomas Bortoli a signalé que le processus du serveur svnserve de
Subversion peut se terminer quand un client envoie certaines séquences de
commandes du protocole. Si le serveur est configuré avec un accès anonyme,
cela pourrait conduire à un déni de service distant non authentifié.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 1.9.5-1+deb9u4.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1.10.4-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets subversion.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de subversion, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/subversion">\
https://security-tracker.debian.org/tracker/subversion</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4490.data"
# $Id: $
