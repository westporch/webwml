#use wml::debian::translation-check translation="c2690338e0b8682aa9f532d5172b9ccdad6025da" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans jupyter-notebook.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8768">CVE-2018-8768</a>

<p>Un fichier notebook malveillant pourrait contourner le nettoyage afin
d’exécuter du Javascript dans le contexte notebook. Précisément, du HTML
incorrect est <q>corrigé</q> par jQuery après le nettoyage, le rendant
dangereux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19351">CVE-2018-19351</a>

<p>Un script intersite est permis à l'aide d'un notebook non fiable parce que
les réponses nbconvert sont considérées comme ayant la même origine que le
serveur de notebook.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21030">CVE-2018-21030</a>

<p>jupyter-notebook n’utilise pas d’en-tête CSP pour traiter les fichiers
servis comme appartenant à une origine distincte. Ainsi, une charge XSS peut
être placée dans un document SVG.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.2.3-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jupyter-notebook.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jupyter-notebook, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jupyter-notebook">https://security-tracker.debian.org/tracker/jupyter-notebook</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2432.data"
# $Id: $
