#use wml::debian::translation-check translation="a9ca05c1d1949a2365e001488e969b21381af18a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans drupal7, une
plateforme PHP pour site web. Ces vulnérabilités affectent les versions
embarquées de la bibliothèque JavaScript jQuery et la bibliothèque d’enveloppe
de flux Phar de Typo3.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11358">CVE-2019-11358</a>

<p>La version de jQuery embarqué dans Drupal était prédisposée à une
vulnérabilité de script intersite dans jQuery.extend().</p>

<p>Pour des informations supplémentaires, veuillez vous référez à l’annonce
amont sur <a href="https://www.drupal.org/sa-core-2019-006.">https://www.drupal.org/sa-core-2019-006.</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11831">CVE-2019-11831</a>

<p>Un validation incomplète dans la bibliothèque de traitement Phar embarquée
dans Drupal, un cadriciel de gestion de contenu complet, pourrait aboutir
à une divulgation d'informations.</p>

<p>Pour des informations supplémentaires, veuillez vous référez à l’annonce
amont sur <a href="https://www.drupal.org/sa-core-2019-007.">https://www.drupal.org/sa-core-2019-007.</a></p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 7.32-1+deb8u17.</p>
<p>Nous vous recommandons de mettre à jour vos paquets drupal7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1797.data"
# $Id: $
