#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Agostino Sarubbo de Gentoo a découvert plusieurs vulnérabilités de
sécurité dans libarchive, une bibliothèque d'archive et de compression
multi-format. Un attaquant pourrait tirer avantage de ces défauts pour
provoquer un dépassement de tampon ou une lecture hors limites en utilisant
un fichier d'entrée soigneusement contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8687">CVE-2016-8687</a>

<p>Agostino Sarubbo de Gentoo a découvert un possible dépassement de pile
lors de l'affichage d'un nom de fichier dans bsdtar_expand_char() de
util.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8688">CVE-2016-8688</a>

<p>Agostino Sarubbo de Gentoo a découvert une possible lecture hors limites
lors de l'analyse de longues lignes multiples dans bid_entry() et
detect_form() d'archive_read_support_format_mtree.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8689">CVE-2016-8689</a>

<p>Agostino Sarubbo de Gentoo a découvert un possible dépassement de tas
lors de la lecture de fichiers 7z corrompus dans read_Header()
d'archive_read_support_format_7zip.c.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.0.4-3+wheezy5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libarchive.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-661.data"
# $Id: $
