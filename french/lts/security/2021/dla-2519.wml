#use wml::debian::translation-check translation="04abf1830d2945ee74e96edb5e11e9a2fd9a1f60" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été corrigées dans pacemaker, un
gestionnaire de ressources de grappe de machines.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16877">CVE-2018-16877</a>

<p>Un défaut a été découvert dans la façon dont l’authentification
client-serveur de pacemaker était implémentée. Un attaquant local pourrait
utiliser ce défaut, et en combinaison avec d’autres faiblesses d’IPC, parvenir
à une élévation locale des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16878">CVE-2018-16878</a>

<p>Une vérification insuffisante affectant la préférence de processus non
contrôlés peut conduire à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25654">CVE-2020-25654</a>

<p>Un défaut de contournement d’ACL a été découvert dans pacemaker. Un attaquant,
ayant un compte local dans la grappe et dans le groupe haclient, pourrait
utiliser une communication IPC avec divers démons directement pour réaliser
certaines tâches qui auraient été empêchées par les ACL si elles étaient issues
de la configuration.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.1.24-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pacemaker.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pacemaker, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pacemaker">https://security-tracker.debian.org/tracker/pacemaker</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2519.data"
# $Id: $
