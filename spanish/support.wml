#use wml::debian::template title="Ayuda técnica"
#use wml::debian::toc
#use wml::debian::translation-check translation="76ce7e7f5cfd0e2f3e8931269bdcd1f9518ee67f"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian y su ayuda técnica se mantienen por su comunidad de voluntarios.

Si esta ayuda dinamizada por la comunidad no cumple sus necesidades, puede
leer nuestra <a href="doc/">documentación</a> o
contratar a un <a href="consultants/">consultor o consultora</a>.

<toc-display />


<toc-add-entry name="irc">Ayuda en línea en tiempo real usando IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> es
una forma de hablar con personas a lo largo de todo el mundo en tiempo
real.
Puede encontrar canales dedicados a Debian en
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Para conectarse necesita un cliente de IRC. Algunos de los más
populares son
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> y
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
todos ellos disponibles para Debian. OFTC también ofrece una interfaz
web <a href="https://www.oftc.net/WebChat/">WebChat</a>
que permite conectarse a IRC con un navegador web sin necesidad de 
instalar ningún cliente local.</p>

<p>Una vez que tenga el cliente instalado
necesitará decirle que se conecte al servidor. En la mayoría lo podrá hacer
con:</p>

<pre>
/server irc.debian.org
</pre>

<p>En algunos clientes (como irssi) necesitará en cambio escribir lo siguiente:</p>

<pre>
/connect irc.debian.org
</pre>

<p>Cuando se conecte, únase al canal <code>#debian</code> (en inglés) escribiendo:</p>

<pre>
/join #debian
</pre>

<p>O al canal <code>#debian-es</code> (en castellano) escribiendo:</p>

<pre>
/join #debian-es
</pre>

<p>Nota: los clientes como HexChat a menudo tienen una interfaz gráfica de
usuario diferente para entrar en servidores/canales.</p>

<p>A partir de este momento se encontrará entre un grupo de amistosos
habitantes tanto de <code>#debian</code> como de <code>#debian-es</code>. Para
conocer las reglas de los distintos canales, consulte
<url "https://wiki.debian.org/DebianIRC" />.</p>

<p>Hay otras redes de IRC donde también puede charlar sobre Debian.</p>

<toc-add-entry name="mail_lists" href="MailingLists/">Listas de Correo</toc-add-entry>

<p>Debian se desarrolla gracias al trabajo de muchas personas
repartidas a lo largo de todo el mundo. Por ello, el correo electrónico
es el método preferido para discutir los distintos asuntos.
La gran mayoría de las conversaciones entre desarrolladores y usuarios
de Debian se gestionan a través de varias listas de correo.</p>

<p>Hay un gran número de listas públicas disponibles. Para más información,
visite la página de
<a href="MailingLists/">listas de correo de Debian</a>.</p>

# Nota a los traductores:
# Tal vez quieras adaptar el siguiente parágrafo, indicando qué lista
# está disponible para los usuarios en tu idioma en vez de en inglés.
<p>
Para obtener ayuda técnica en español, por favor escriba a la lista de correo 
<a href="https://lists.debian.org/debian-user-spanish/">debian-user-spanish
</a>.
</p>

<p>
Para ayuda técnica en otros idiomas, revise el
<a href="https://lists.debian.org/users.html">índice
de listas de correo para usuarios</a>.
</p>

<p>Por supuesto, hay también muchas otras listas de correo dedicadas a algún aspecto del
enorme y vasto ecosistema de Linux, y que no son específicas de Debian. Use su motor de
búsqueda favorito para encontrar la lista más adecuada a su propósito.</p>


<toc-add-entry name="usenet">Grupos de noticias Usenet</toc-add-entry>

<p>Muchas de nuestras <a href="#mail_lists">listas de correo</a> las puede
leer como grupos de noticias, en la jerarquía <kbd>linux.debian.*</kbd>.
También lo puede hacer mediante una interfaz web como
<a href="https://groups.google.com/forum/">Google Groups</a>.</p>


<toc-add-entry name="web">Sitios web</toc-add-entry>

<h3 id="forums">Foros</h3>

<p><a href="https://exdebian.org/forum">Foro de usuarios de Debian en español</a> 

<p><a href="http://forums.debian.net">Foros de usuarios de Debian</a> es un portal
web al que puede enviar preguntas sobre Debian (en inglés), que serán
contestadas por otros usuarios.</p>


<toc-add-entry name="maintainers">Contactar con los desarrolladores de paquetes</toc-add-entry>

<p>Hay dos formas de contactar con los desarrolladores de paquetes. Si
necesita contactar con uno debido a un fallo, simplemente envíe un
informe de fallo (consulte la sección del Sistema de seguimiento de fallos
más abajo). El desarrollador recibirá una copia de este.</p>

<p>Si quiere simplemente comunicarse con el desarrollador, entonces
puede usar los alias de correo especial instalados para cada paquete.
Cualquier correo enviado a
&lt;<em>nombre de paquete</em>&gt;@packages.debian.org se reenviará
al desarrollador responsable de ese paquete.</p>


<toc-add-entry name="bts" href="Bugs/">Sistema de seguimiento de fallos</toc-add-entry>

<p>La distribución Debian tiene un sistema de seguimiento de
fallos («bugs») que detalla fallos remitidos por usuarios y
desarrolladores. Cada fallo recibe un número, y se guarda registro
hasta que se marca como solucionado.</p>

<p>Para enviar un fallo, puede leer la página listada
abajo; recomendamos el uso del paquete Debian <q>reportbug</q> para enviar
automáticamente un informe de fallo.</p>

<p>En <a href="Bugs/">las páginas del sistema de
seguimiento de fallos</a> se puede obtener información 
sobre el envío de fallos, revisión de los fallos
activos actualmente, y del sistema de seguimiento de fallos en
general.</p>


<toc-add-entry name="consultants" href="consultants/">Consultores</toc-add-entry>

<p>Debian es software libre y ofrece ayuda gratuita a través de las
listas de distribución. Algunas personas o bien no tienen el tiempo
necesario o tienen necesidades especiales y están dispuestas a pagar a
alguien para mantener o añadir funcionalidad a su sistema Debian. Consulte
las <a href="consultants/">páginas de consultores</a> para una
lista de personas y empresas.</p>


<toc-add-entry name="release" href="releases/stable/">Problemas conocidos</toc-add-entry>

<p>Las limitaciones y problemas graves de la distribución estable actual
(si los hay) se detallan en <a href="releases/stable/">las páginas de
la distribución</a>.</p>

<p>Preste atención especial a las
<a href="releases/stable/releasenotes">notas de publicación</a> y las
<a href="releases/stable/errata">erratas</a>.</p>
