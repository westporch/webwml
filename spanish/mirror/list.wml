#use wml::debian::template title="Réplicas de Debian en todo el mundo" BARETITLE=true
#use wml::debian::translation-check translation="54e7b1c853358a6c386f31e4ac3a5d90a6f3890f" maintainer="Laura Arjona Reina"

<p>Debian se distribuye (mediante <em>réplicas</em>) a través de
cientos de servidores en Internet. Usar un servidor cercano ayuda a
acelerar la descarga a la vez que se reduce la carga en nuestros
servidores centrales así como en la propia Internet en general.</p>

<p class="centerblock">
  Existen réplicas de Debian en muchos países, y para algunas hemos agregado
  un alias <code>ftp.&lt;país&gt;.debian.org</code>.  Este alias usualmente
  apunta a una réplica que se sincroniza regularmente y rápidamente, y proporciona
  todas las arquitecturas de Debian. El repositorio de Debian siempre está disponible
  vía <code>HTTP</code> en la ubicación <code>/debian</code>
  del servidor.
</p>

<p class="centerblock">
  Otras <strong>réplicas</strong> pueden tener restricciones en qué replican
  (debido a limitaciones de espacio). Simplemente porque un sitio no es el alias
  <code>ftp.&lt;país&gt;.debian.org</code> de un país no significa necesariamente
  que sea más lento o que esté menos actualizado que la réplica
  <code>ftp.&lt;país&gt;.debian.org</code>.
  De hecho, una réplica que contenga su arquitectura y esté más cerca al usuario,
  y por tanto sea más rápida, casi siempre es preferible frente a otras réplicas
  que estén más lejos.
</p>

<p>Para una descarga lo más rápida posible use la réplica más cercana
a usted, ya sea esta una réplica con alias de país o no.
Use el
programa <a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> para determinar cuál es la réplica con menor
latencia; use un programa de descarga como
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> o bien
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> para determinar la réplica que proporciona mejor
tasa de transferencia. Tenga en cuenta que la proximidad geográfica a 
menudo no es el factor más importante a la hora de determinar la máquina
que puede ofrecerle un mejor servicio.</p>

<p>
Si su sistema cambia mucho de ubicación, quizá le sirva bien una «réplica»
que esté respaldada por una red de distribución de contenidos 
(<abbr title="Content Delivery Network">CDN</abbr>) global.
El proyecto Debian mantiene 
<code>deb.debian.org</code> para este propósito y puede usarlo en su
archivo sources.list de apt &mdash; consulte
<a href="http://deb.debian.org/">el sitio web del servicio para más detalles</a>.

<p>La copia de referencia de la siguiente lista siempre se puede
consultar en:
<url "https://www.debian.org/mirror/list">.
Consulte la página
<url "https://www.debian.org/mirror/">
para todo lo demás que quiera saber sobre réplicas.
</p>

<h2 class="center">Direcciones de las réplicas de Debian con alias de país</h2>

<table border="0" class="center">
<tr>
  <th>País</th>
  <th>Dirección</th>
  <th>Arquitecturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Lista de réplicas del repositorio de Debian</h2>

<table border="0" class="center">
<tr>
  <th>Nombre de la máquina</th>
  <th>HTTP</th>
  <th>Arquitecturas</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
